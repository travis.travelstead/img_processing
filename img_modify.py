import sys
sys.dont_write_bytecode = True

import shutil
import platform
import os
import pathlib

from PIL import Image

from wand.image import Image


def image_depth_dither_spread(the_image, result='to/result.png', depth=64, spread_pixels=4, spread_count=3):
    with Image(filename=the_image) as img:
        img.posterize(levels=depth, dither='floyd_steinberg')
        for count in range(spread_count):
            img.spread(radius=spread_pixels)

        img.save(filename=result)


def image_depth_dither(the_image, result='to/result.png', depth=64, spread_pixels=4, spread_count=3):
    with Image(filename=the_image) as img:
        img.posterize(levels=depth, dither='floyd_steinberg')

        img.save(filename=result)


# img = Image.open('from/Blur.png')
# print(img.format, img.size, img.mode, img.getbands())
# print(img.getpalette())

# img.show()

# img2 = img.convert('RGBA', dither=Image.FLOYDSTEINBERG, palette=Image.ADAPTIVE, colors=128)
# img2 = img.quantize(colors=4, palette=None, dither=1)
# img2 = img.quantize()
# print(img2.format, img2.size, img2.mode, img2.getbands())
# img2.save('to/img2.png')
# img2.show()

# img3 = img2.convert('RGBA')
# print(img3.format, img3.size, img3.mode, img3.getbands())
# img3.putalpha(img.getbands().index('A'))
# img3.save('to/img3.png')
# img3.show()