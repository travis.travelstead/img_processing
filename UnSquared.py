import sys
sys.dont_write_bytecode = True

import shutil
import platform
import os
import pathlib

from PIL import Image
from cmd import Cmd


class UnSquared(Cmd):

    def __init__(self):
        Cmd.__init__(self)

        self.VERSION = '0.0.1'
        self.plat = platform.system()
        self.DIR_FROM = 'From/'
        self.DIR_TO = 'To/'
        self.TYPE_PNG = '*.png'
        self.TYPE_JPEG = '*.jpeg'
        self.TYPE_JPG = '*.jpg'

        self.filelist = []

        self.prompt = '(UnSquared) >> '
        self.INTRO = 'UnSquared ' + self.VERSION + ' Running on ' + self.plat

    def do_exit(self, args):
        """
        CMD - Exit deployment prompt
        """
        print('Exiting deploy prompt')
        print('')
        return True

    def do_check(self, args):
        print('Check From')
        self.check_dir(self.DIR_FROM)
        self.check_files(self.DIR_FROM, add_to_list=True)
        print('Check To')
        self.check_dir(self.DIR_TO)
        self.check_files(self.DIR_TO)
        print('')
        print(self.filelist)

    def do_unsquare(self, args):
        print('Unsquare images')
        if self.filelist is not None:
            for file_path in self.filelist:
                print(file_path)
                destination = file_path.name
                print(destination)
                self.file_resize_mult(file_path, (self.DIR_TO + destination), 1, 0.93)

    def check_dir(self, the_dir):
        if os.path.exists(the_dir):
            return '{} already exists'.format(the_dir)
        else:
            os.makedirs(the_dir)
            return '{} did not exist and was created'.format(the_dir)

    def check_files_type(self, the_dir, filetype):
        """
        """
        print('Direction {}'.format(the_dir))
        for the_file in pathlib.Path(the_dir).glob(filetype):
            print(the_file)
            self.filelist.append(the_file)

    def check_files(self, the_dir, add_to_list=False):
        print('Directory - {}'.format(the_dir))
        if add_to_list is True:
            self.filelist = []
        for the_file in pathlib.Path(the_dir).iterdir():
            print(the_file)
            if add_to_list is True:
                self.filelist.append(the_file)

    def file_resize_dims(self, path_from_with_name, path_to_with_name, x_width, y_height):
        img_origin = Image.open(path_from_with_name)
        img_skewed = img_origin.resize((x_width, y_height), Image.ANTIALIAS)
        img_skewed.save(path_to_with_name, format=None, optimize=True)

    def file_resize_mult(self, path_from_with_name, path_to_with_name, x_mult, y_mult):
        img_origin = Image.open(path_from_with_name)
        img_origin_size = img_origin.size
        print('Original image size')
        print(img_origin.size)
        img_skewed = img_origin.resize((int((img_origin_size[0] * x_mult)), int((img_origin_size[1] * y_mult))), Image.ANTIALIAS)
        print('Modified size')
        print(img_skewed.size)
        img_skewed.save(path_to_with_name, format=None, optimize=True)


if __name__ == '__main__':
    print()
    print()
    prompt = UnSquared()
    prompt.cmdloop(intro=prompt.INTRO)